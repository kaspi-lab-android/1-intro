package kz.company.testappone

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val countStatic = 0
        var countNonStatic = 0

        val text: String = "text"
        val num: Double = 1.5
        val stringArray: List<String>
        val numArray: List<Int>
        val numMutableArray: MutableList<Int> = mutableListOf()

        button.setOnClickListener {
            countNonStatic++
            numMutableArray.add(countNonStatic)
            textView.text = "Button clicked $countNonStatic $numMutableArray"



            numMutableArray.forEach {

            }
            numMutableArray.forEachIndexed { index, i ->

            }

            for (i: Int in 0 until 10) {
                val random = Random.nextInt(0, 10)
                numMutableArray.add(random)
            }
        }
    }

    fun analysisText(){
        val text = "azat"
        val result = "a - 2(50%) z - 1(25%) t - 1(25%)"
        textView.text = result
    }
}
